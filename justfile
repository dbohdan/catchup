set dotenv-load := true

project := "catchup"
binary := "bin" / project
deb-file := project + ".deb"
temp-dir := "/tmp" / project

default: test

static:
    docker run \
        -it \
        --rm \
        --mount type=bind,source={{ quote(justfile_directory()) }},target=/build \
        crystallang/crystal:1.12.1-alpine \
        sh -c "cd /build \
            && shards install \
            && shards build --release --static \
            && strip {{ quote(binary) }}" \
        ;

deb:
    -rm -rf {{ quote(temp-dir) }}

    mkdir -m 700 {{ quote(temp-dir) }}
    cp -a -r debian/* {{ quote(temp-dir) }}
    mkdir -p {{ quote(temp-dir) }}/opt/{{ quote(project) }}/bin/
    cp -a {{ quote(binary) }} {{ quote(temp-dir) }}/opt/{{ quote(project) }}/bin/
    mkdir -p {{ quote(temp-dir) }}/etc/opt/{{ quote(project) }}/
    @echo "postgres://catchup:$PASSWORD@localhost:5432/catchup" > {{ quote(temp-dir) }}/etc/opt/{{ quote(project) }}/pg-uri

    dpkg-deb --root-owner-group --build {{ quote(temp-dir) }} {{ quote(deb-file) }}

deploy:
    rsync --progress --rsh "ssh -p '$SSH_PORT'" {{ quote(deb-file) }} "$SSH_SERVER":/tmp/{{ quote(deb-file) }}
    ssh -p "$SSH_PORT" -t "$SSH_SERVER" sudo dpkg -i /tmp/{{ quote(deb-file) }}

clean:
    -rm -f {{ quote(deb-file) }}

test:
    crystal spec
