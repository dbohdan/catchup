require "./spec_helper"

describe ArrayUtils do
  it "groups all distinct elements correctly" do
    ArrayUtils(String).group_by_first(["a", "b", "c"]) do |ref, x|
      ref == x
    end.should eq [["a"], ["b"], ["c"]]
  end

  it "groups all identical elements correctly" do
    ArrayUtils(String).group_by_first(["a", "b", "c"]) do |ref, x|
      true
    end.should eq [["a", "b", "c"]]
  end
end
