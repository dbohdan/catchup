require "./spec_helper"

include CatchUp

describe CLI do
  it "parses IRC URLs" do
    ref = {
      irc_channel: "#channel",
      irc_host:    "example.com",
      irc_port:    6667,
      irc_tls:     false,
    }

    CLI.parse_irc_uri("irc://example.com/#channel").should eq ref
  end

  it "parses secure IRC URLs" do
    ref = {
      irc_channel: "#channel",
      irc_host:    "irc.example.com",
      irc_port:    6697,
      irc_tls:     true,
    }

    CLI.parse_irc_uri("ircs://irc.example.com/#channel").should eq ref
  end

  it "parses port numbers" do
    ref = {
      irc_channel: "##foo-offtopic",
      irc_host:    "example.org",
      irc_port:    12345,
      irc_tls:     true,
    }

    CLI.parse_irc_uri("ircs://example.org:12345/##foo-offtopic").should eq ref
  end

  it "doesn't parse garbage" do
    CLI.parse_irc_uri("asdf://example.com").should eq nil
  end

  it "allows only one channel" do
    CLI.parse_irc_uri("irc://example.com/#ch1,#ch2").should eq nil
  end
end
