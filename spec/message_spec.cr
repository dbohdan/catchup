require "./spec_helper"

describe Message do
  it "unwraps JoinPart messages 1" do
    Message.unwrap_bridged_message(
      /bridge/,
      "\u0001ACTION fooey_mcbar has become available\u0001"
    ).should eq({
      Message::Type::JoinPart,
      "fooey_mcbar",
      "has become available",
    })
  end

  it "unwraps JoinPart messages 2" do
    Message.unwrap_bridged_message(
      /bridge/,
      "\u0001ACTION fooey_mcbar has left\u0001"
    ).should eq({Message::Type::JoinPart, "fooey_mcbar", "has left"})
  end

  it "unwraps JoinPart messages 3" do
    Message.unwrap_bridged_message(
      /bridge/,
      "\u0001ACTION *** fooey_mcbar joins\u0001"
    ).should eq({Message::Type::JoinPart, "fooey_mcbar", "joins"})
  end

  it "unwraps JoinPart messages 4" do
    Message.unwrap_bridged_message(
      /bridge/,
      "\u0001ACTION *** fooey_mcbar leaves\u0001"
    ).should eq({Message::Type::JoinPart, "fooey_mcbar", "leaves"})
  end
end
