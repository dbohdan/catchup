require "baked_file_system"
require "kilt"
require "kilt/ecr"
require "memory_cache"
require "safe_ecr"

module CatchUp::Web
  extend self

  class FileStorage
    extend BakedFileSystem

    bake_folder "../public"
  end

  class IndexView
    def initialize(@channel : String, @grouped : Array(Array(Message)))
    end

    Kilt.file("src/index.html.ecr")
  end

  def start_server(
    channel : String,
    chat_log : ChatLog
  )
    view_cache = MemoryCache(Array(Message) | String, String).new

    HTTP::Server.new do |context|
      path = context.request.path
      if path == "/"
        msgs = chat_log.values

        html = view_cache.fetch(msgs, expires_in: 1.minute) {
          grouped = ArrayUtils(Message).group_by_first(msgs) do |ref, x|
            x.type == ref.type &&
              x.bridge == ref.bridge &&
              x.user == ref.user &&
              (!x.type.user_message? || x.time - ref.time <= 5.minutes)
          end

          IndexView.new(channel, grouped).to_s
        }

        context.response.content_type = "text/html"
        context.response.headers.add("Cache-Control", "max-age=60")
        context.response.print(html)
      elsif path == "/json"
        json = view_cache.fetch("/json", expires_in: 1.minute) {
          chat_log.json
        }

        context.response.content_type = "application/json"
        context.response.headers.add("Cache-Control", "max-age=60")
        context.response.print(json)
      else
        asset = FileStorage.get?(path)
        if asset
          context.response.content_type =
            begin
              MIME.from_filename(path)
            rescue KeyError
              "application/octet-stream"
            end

          context.response.print(asset.gets_to_end)
        else
          context.response.content_type = "text/plain"
          context.response.status = HTTP::Status::NOT_FOUND
          context.response.print("File not found.")
        end
      end
    end
  end
end
