module ArrayUtils(T)
  extend self

  def group_by_first(xs : Array(T), &block)
    return [] of Array(T) if xs.empty?

    grouped = [] of Array(T)
    ref = xs[0]
    group = [ref]

    xs.skip(1).each do |x|
      if yield(ref, x)
        group.push(x)
      else
        grouped.push(group)
        ref = x
        group = [ref]
      end
    end

    grouped.push(group)
    grouped
  end
end
