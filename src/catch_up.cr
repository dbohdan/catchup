require "./array_utils"
require "./chat_log"
require "./cli"
require "./database"
require "./irc"
require "./message"
require "./web"

module CatchUp
  VERSION = "0.14.0"
end
