module CatchUp::Database
  extend self

  def migrate(pg_conn : PG::Connection, table : String)
    pg_conn.exec_all(
      # We have to interpolate the table name.
      <<-SQL
        CREATE TABLE IF NOT EXISTS "#{table}"(
          "timestamp" timestamp with time zone PRIMARY KEY,
          "type" text NOT NULL,
          "bridge" text NOT NULL,
          "user" text NOT NULL,
          "text" text NOT NULL
        );
        DO $$
          BEGIN
            BEGIN
              ALTER TABLE "#{table}"
              ADD COLUMN "bridge" text NOT NULL DEFAULT '';
            EXCEPTION
              WHEN duplicate_column THEN -- Do nothing.
            END;

            BEGIN
              ALTER TABLE "#{table}"
              ADD COLUMN "type" text NOT NULL DEFAULT 'Message';
            EXCEPTION
              WHEN duplicate_column THEN -- Do nothing.
            END;

            ALTER TABLE "#{table}"
            DROP COLUMN IF EXISTS "carrier";
          END
        $$
      SQL
    )
  end
end
