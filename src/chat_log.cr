class CatchUp::ChatLog
  def initialize(
    @db : DB::Database,
    @table : String
  )
  end

  def add(msg : Message)
    sql = <<-SQL
      INSERT INTO "#{@table}"(
        "timestamp",
        "type",
        "bridge",
        "user",
        "text"
      ) VALUES ($1, $2, $3, $4, $5);
    SQL

    @db.exec(sql, msg.time, msg.type, msg.bridge, msg.user, msg.text)

    self
  end

  def delete_old(older_than : Time::Span)
    @db.exec(
      <<-SQL
        DELETE FROM "#{@table}"
        WHERE "timestamp" < now() - INTERVAL '#{older_than.to_i} seconds'
      SQL
    )
  end

  def json
    blob = ""

    @db.query(
      <<-SQL
        SELECT json_agg(row_to_json(row, true)) #>> '{}' FROM (
          SELECT "timestamp", "type", "bridge", "user", "text"
          FROM "#{@table}"
          WHERE "type" IN ('Action', 'Message', 'SessionStart')
          ORDER BY "timestamp" ASC
        ) row;
      SQL
    ) do |rs|
      rs.each do
        blob = rs.read(String)
      end
    end

    blob
  end

  def values
    msgs = Array(Message).new

    @db.query(
      <<-SQL
        SELECT "timestamp", "type", "bridge", "user", "text"
        FROM "#{@table}"
        ORDER BY "timestamp" ASC;
      SQL
    ) do |rs|
      rs.each do
        time = rs.read(Time)
        msg_type = Message::Type.parse(rs.read(String))
        bridge = rs.read(String)
        user = rs.read(String)
        text = rs.read(String)

        msgs << Message.new(time, msg_type, bridge, user, text)
      end
    end

    msgs
  end
end
