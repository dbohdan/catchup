require "option_parser"

class CatchUp::CLI
  def self.parse
    _irc_bridge = "^i[js]chain"
    _irc_uri = "ircs://irc.libera.chat/#tcl"

    http_host = "127.0.0.1"
    http_port = 8080
    irc_bridge = /x^/
    irc_channel = "#tcl"
    irc_host = ""
    irc_nick = "CatchUpBot"
    irc_port = -1
    irc_tls = false
    pg_table = "tcl"
    pg_uri = "postgres://catchup:password@localhost:5432/catchup"
    pg_uri_file = ""

    begin
      OptionParser.parse do |parser|
        parser.banner = "Usage: catchup [options]"
        parser.on("--host #{http_host}", "Host to bind to") do |x|
          http_host = x
        end
        parser.on("--port #{http_port}", "Port to bind to") do |x|
          http_port = x.to_i
        end
        parser.on("--irc-uri '#{_irc_uri}'", "IRC connection URI") do |x|
          _irc_uri = x
        end
        parser.on("--nick #{irc_nick}", "IRC bot's nick") do |x|
          irc_nick = x
        end
        parser.on("--bridge '#{_irc_bridge}'",
          "Matching nicks are treated as bridges to other channels") do |x|
          _irc_bridge = x
        end
        parser.on("--pg-table #{pg_table}",
          "PostgreSQL database table") do |x|
          pg_table = x
        end
        parser.on("--pg-uri #{pg_uri}",
          "PostgreSQL connection URI (read from CATCHUP_PG_URI if \
           empty)") do |x|
          pg_uri = x
        end
        parser.on("--pg-uri-file ''",
          "File to read PostgreSQL connection URI from") do |x|
          pg_uri_file = x
        end
        parser.on("-v", "--version", "Print version and exit") do
          puts VERSION
          exit
        end
        parser.on("-h", "--help", "Show this help and exit") do
          puts parser
          exit
        end
      end

      irc_bridge = Regex.new(_irc_bridge)

      irc_connection = parse_irc_uri(_irc_uri)
      if !irc_connection
        raise "Cannot parse IRC URI: #{_irc_uri}"
      end
    rescue ex
      STDERR.puts ex.message
      exit(1)
    end

    config = {
      http_host:   http_host,
      http_port:   http_port,
      irc_bridge:  irc_bridge,
      irc_nick:    irc_nick,
      pg_table:    pg_table,
      pg_uri:      pg_uri,
      pg_uri_file: pg_uri_file,
    }

    config.merge(irc_connection)
  end

  def self.parse_irc_uri(s : String)
    m =
      /^
      (?<proto>ircs?):\/\/
      (?<host>[^:]+)
      (?::(?<port>\d+))?
      \/(?<channel>[^ ,]+)
      $/x.match(s)
    if !m
      return nil
    end

    irc_tls = m["proto"] == "ircs"
    irc_host = m["host"]
    irc_port = (m["port"]? || (irc_tls ? "6697" : "6667")).to_i
    irc_channel = m["channel"]

    {
      irc_channel: irc_channel,
      irc_host:    irc_host,
      irc_port:    irc_port,
      irc_tls:     irc_tls,
    }
  end
end
