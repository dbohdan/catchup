struct CatchUp::Message
  property time, type, bridge, user, text

  @@irc_formatting =
    /(?:\x02|\x03(?:\d{1,2}(?:,\d{1,2})?)?|\x0f|\x11|\x12|\x16|\x1d|\x1f)/

  enum Type
    Action
    JoinPart
    Message
    SessionStart
    Unknown

    def user_message?
      self == Action || self == Message
    end
  end

  def initialize(
    @time : Time,
    @type : Type,
    @bridge : String,
    @user : String,
    @text : String
  )
  end

  def self.unwrap_bridged_message(is_bridge : Regex, msg : String)
    if m =
         /
        ^
        \x{0001}
        ACTION[ ]
        (?:\*\*\*[ ](?<nick>.+)[ ](?<action>joins|leaves)|
         (?<nick>.+)[ ](?<action>has[ ](?:left|become[ ]available)))
        \x{0001}
        $
      /x.match(msg)
      return {Type::JoinPart, m["nick"], m["action"]}
    end

    msg_type = Type::Unknown
    user = ""
    text = ""

    if m = (/^\x{0001}ACTION ([^ ]+) (.*)\x{0001}$/.match(msg) ||
            /^\* ([^ ]+) (.*)$/.match(msg))
      msg_type = Type::Action
      _, user, text = m
    elsif m = /^[«<]([^>»]+)[>»] (.*)$/.match(msg)
      msg_type = Type::Message
      _, user, text = m
    end

    if is_bridge.match(user)
      # Handle nested bridges.
      if msg_type == Type::Action
        text = "\x01ACTION #{text}\x01"
      end
      return unwrap_bridged_message(is_bridge, text)
    end

    {msg_type, user, text}
  end

  def self.from_fast_irc(
    irc_msg : FastIRC::Message,
    is_bridge : Regex
  )
    if irc_msg.command != "PRIVMSG"
      raise Exception.new("Not a PRIVMSG: #{irc_msg}")
    end
    if !irc_msg.prefix
      raise Exception.new("No prefix: #{irc_msg}")
    end

    msg_type = Type::Message
    time = Time.utc
    bridge = ""
    user = irc_msg.prefix.not_nil!.source
    text = irc_msg.params[1]

    if is_bridge.match(user)
      bridge = user

      msg_type, user, b_text = unwrap_bridged_message(is_bridge, text)
      if msg_type != Type::Unknown
        text = b_text
      end
    elsif action_m = /^\x{0001}ACTION (.*)\x{0001}$/.match(text)
      msg_type = Type::Action
      text = action_m[1]
    end

    Message.new(time, msg_type, bridge, user, text)
  end

  def plain_text
    @text.gsub(@@irc_formatting, "")
  end
end
