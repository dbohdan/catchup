require "./catch_up"

require "db"
require "pg"

include CatchUp

config = CLI.parse
Log.setup_from_env
Log.debug { "#{config}" }

pg_uri = config[:pg_uri]
if pg_uri == ""
  pg_uri = ENV["CATCHUP_PG_URI"]
end
if config[:pg_uri_file] != ""
  pg_uri = File.read(config[:pg_uri_file])
end

DB.open(pg_uri) do |db|
  db.using_connection do |conn|
    Database.migrate(conn.as(PG::Connection), config[:pg_table])
  end

  chat_log = ChatLog.new(db, config[:pg_table])

  spawn name: "IRC client" do
    begin
      chat_log.add(Message.new(
        Time.utc,
        Message::Type::SessionStart,
        "",
        config[:irc_nick],
        "Connecting"
      ))

      irc_client = IRC::Client.new(
        config[:irc_host],
        config[:irc_port],
        config[:irc_tls],
        config[:irc_nick],
        config[:irc_channel]
      ) do |irc_msg|
        if irc_msg.command == "PRIVMSG" &&
           irc_msg.params[0] == config[:irc_channel]
          msg = Message.from_fast_irc(irc_msg, config[:irc_bridge])

          if msg.type == Message::Type::Unknown
            Log.error { "Can't parse message: #{msg}" }
          end

          chat_log.add(msg)
          chat_log.delete_old(Time::Span.new(days: 2))
        end
      end
    rescue ex
      STDERR.puts ex.message
      exit(1)
    end
  end

  server = Web.start_server(config[:irc_channel], chat_log)
  address = server.bind_tcp(config[:http_host], config[:http_port])
  Log.info { "Listening on #{address}" }
  server.listen
end
