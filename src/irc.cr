require "fast_irc"
require "openssl"
require "socket"

module CatchUp::IRC
  Log = ::Log.for("irc")

  class Client
    @last_ping : Time = Time.utc

    def initialize(
      host : String,
      port : Int32,
      tls : Bool,
      nick : String,
      channel : String,
      &block
    )
      Log.debug { "Connecting to #{[host, port]}" }

      client = TCPSocket.new(host, port)
      client.read_timeout = 180
      client.write_timeout = 5
      client.keepalive = true

      actual_server = client.remote_address

      if tls
        client = OpenSSL::SSL::Socket::Client.new(client)
      end

      client.puts "NICK #{nick}\r\n"
      client.puts "USER #{nick} 8 * :CatchUpBot\r\n"
      client.puts "JOIN #{channel}\r\n"
      client.flush

      spawn do
        while Time.utc - @last_ping < 240.seconds
          sleep(60.seconds)

          client.puts "PING #{actual_server}\r\n"
          client.flush
        end

        client.close
      end

      error_regexp = /^[45]\d\d$/

      FastIRC.parse(client) do |message|
        Log.debug { message.inspect }

        if error_regexp.match(message.command)
          raise "IRC error reply: \"#{message.to_s.rstrip}\""
        end

        yield message

        if message.command == "PING"
          FastIRC::Message.new("PONG", message.params).to_s(client)
          client.flush

          @last_ping = Time.utc
          Log.debug { "PING #{message.params}" }
        elsif message.command == "PONG"
          @last_ping = Time.utc
          Log.debug { "PONG #{message.params}" }
        end
      end
    end
  end
end
