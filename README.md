# CatchUp

**CatchUp** is an ephemeral IRC logger and a mobile-friendly Web-based log viewer in a single binary.
It was created for #tcl on freenode (and now Libera Chat), but it is written in [Crystal](https://crystal-lang.org/).
It understands IRC bridges, like the `ijchain` XMPP bridge and the `ischain` Slack bridge that operate on the channel.
It only keeps logs for the last 48 hours.
The goal is to let you catch up with the scrollback from your phone, not to keep a record forever.

CatchUp is more of a rough personal project open-sourced for convenience.
You may run into problems if you use it for something other than I do.

You will need PostgreSQL to run CatchUp.


## Screenshot

![Screenshot.](screenshot.png)


## Building and deploying a static binary

These instructions tell you how to deploy CatchUp to recent Debian or Ubuntu.
They are possibly incomplete.

1\. Install the offical Caddy [deb package](https://github.com/caddyserver/caddy/releases) on the target server.
Replace the default `Caddyfile` with one that imports configuration files from `Caddyfile.d/`.

```sh
sudo mkdir /etc/caddy/Caddyfile.d/
echo 'import Caddyfile.d/*.conf' | sudo tee /etc/caddy/Caddyfile
sudo systemctl restart caddy
```

2\. Install PostgreSQL.
Create a user and a database for CatchUp.

```none
$ sudo -u postgres psql
psql (11.7 (Debian 11.7-0+deb10u1))
Type "help" for help.

postgres=# create database "catchup";
CREATE DATABASE
postgres=# create user "catchup" with encrypted password '<your database password>';
CREATE ROLE
postgres=# grant all privileges on database "catchup" to "catchup";
GRANT
```

3\. Install Docker and [just](https://github.com/casey/just) on your developer machine.
Clone this repository.

4\. Modify and rename `debian/etc/caddy/Caddyfile.d/tclchat.dbohdan.com.conf` for your domain.

5\. Modify `debian/lib/systemd/system/catchup.service` to set the bot's nick, channel, and database configuration if needed.

6\. Create the file `.env` with the contents

```none
SSH_SERVER=<your SSH server host>
SSH_PORT=<your SSH server port>
PASSWORD=<your database password>
```

7\. Run the following shell command to build and deploy CatchUp.

```sh
just static deb deploy clean
```


## Command-line options

```none
Usage: catchup [options]
    --host 127.0.0.1                 Host to bind to
    --port 8080                      Port to bind to
    --irc-uri 'ircs://irc.libera.chat/#tcl'
                                     IRC connection URI
    --nick CatchUpBot                IRC bot's nick
    --bridge '^i[js]chain'           Matching nicks are treated as bridges to other channels
    --pg-table tcl                   PostgreSQL database table
    --pg-uri postgres://catchup:password@localhost:5432/catchup
                                     PostgreSQL connection URI (read from CATCHUP_PG_URI if empty)
    --pg-uri-file ''                 File to read PostgreSQL connection URI from
    -v, --version                    Print version and exit
    -h, --help                       Show this help and exit
```


## License

MIT.
